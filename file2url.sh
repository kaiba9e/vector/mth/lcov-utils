#!/bin/bash
#
# Get the URL of a source file from coverage report
#

#
# XXX: Note the sample URLs are from:
#      https://gitlab.com/cki-project/kpet-db/-/merge_requests/517
#
SAMPLE_PIPELINE='https://s3.upshift.redhat.com/DH-PROD-CKI/internal-artifacts/490187325/test%20x86_64/2193569466'
SAMPLE_INDEX_URL="$SAMPLE_PIPELINE/artifacts/coverage/report/index.html"
SAMPLE_RAW_FILE_URL="$SAMPLE_PIPELINE/artifacts/coverage/kcovcomb_sanitized.info"

cov_report_index_url=${1?"*** coverage index.html, e.g. $SAMPLE_INDEX_URL"}
prefix=$(dirname $cov_report_index_url)
surfix=".gcov.html"
file_path=${2?"*** e.g. arch/x86/mm/mmap.c"}
echo
echo "$prefix/$file_path$surfix" | sed 's/^/    /g'
echo
