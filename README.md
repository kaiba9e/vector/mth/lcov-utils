# LCOV Utilities

A utility to parse [LCOV][01] info file, including covert it to JSON format.

[01]: https://github.com/linux-test-project/lcov
